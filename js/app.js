const $canvasContainer = $(".canvas-container");
const $appContainer = $(".app-container");
const boxWidth = 65;
const robotWidth = 152;
const robotHeight = 164;
const boxes = [];

// first we need to create a stage
const stage = new Konva.Stage({
  container: '.canvas-container',
  width: $canvasContainer.innerWidth(),
  height: $canvasContainer.innerHeight()
});

// then create layer
const layer = new Konva.Layer();

// add the layer to the stage
stage.add(layer);

const box = new Konva.Rect({
  x: 0,
  y: stage.getHeight() - 200,
  width: boxWidth,
  height: 100,
  fill: 'green',
  stroke: 'black',
  strokeWidth: 4
});
boxes.push(box);
layer.add(box);
layer.draw();

// robot
let robot;
const robotImage = new Image();
robotImage.src = "img/robbie-05@2x.png";
robotImage.onload = () => {
  robot = new Konva.Image({
    x: ($canvasContainer.innerWidth() - boxWidth) / 2 - robotWidth / 2 + boxWidth,
    y: $canvasContainer.innerHeight() - robotHeight,
    image: robotImage,
    width: robotWidth,
    height: robotHeight
  });
  layer.add(robot);
  layer.draw();
};

function redraw() {
  robot.y($canvasContainer.innerHeight() - robotHeight);
  stage.setWidth($canvasContainer.innerWidth());
  stage.setHeight($canvasContainer.innerHeight());
}

window.addEventListener("resize", redraw);